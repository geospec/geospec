# ImgSPEC

### Overview
The Geospatial Imaging Spectroscopy Processing Environment on the Cloud (ImgSPEC) is a research and development prototype sponsored by the NASA Earth Science Science Technology Office (ESTO) Advanced Information System Technology (AIST) program. This program “identifies, develops, and supports adoption of software and information systems, as well as novel computer science technologies expected to be needed by the [NASA] Earth Science Division in the 5-10-year timeframe.”

### Motivation

NASA DAACs (Distributed Active Archive Centers) and other data archives have troves of science data that is available 
for scientists to analyze. Over time the size of such datasets has outgrown the capacity of most personal computers. 
ImgSPEC aims to bring science closer to the data by providing a cloud based algorithm development and analysis platform.


### Architecture
ImgSPEC’s system design leverages existing technologies (Technology Readiness Level 3+) including:

- Multi-mission Algorithm and Analysis Platform (MAAP) – a coding user interface for launching user-preferred workspaces like Jupyter notebooks with access to scalable backend cloud computing
- Hybrid Science Data System (HySDS) – a hybrid data system that optimizes compute across cloud and on-prem resources
- EcoSIS – a open, community-curated spectral library repository 
- EcoSML – a curated repository of open-source code and algorithms for working with spectroscopy data
- Common Mapping Client – a web mapping service
- NASA Earthdata Search – faceted data search and subsetting to enable data discovery
- NASA Common Metadata Repository – a database of metadata for NASA data search, discovery, and access

#### System Architecture

![Architecture](images/ImgSPEC-SystemDesign-20220127.png)


### Deployment

We will split the deployment steps into different subsystems, namely
- ADE: Algorithm Development Environment that includes technologies like EclipseChe, JupyterLab, docker, gitlab. 
  [Click here for instructions for ADE deployment](installation/install-ade.md)
- Earthdatasearach and CMR (common metadata repository) to allow users to store and search the data.
  
  NASA's Common Metadata Repository (CMR) is a high-performance, high-quality, continuously evolving metadata system 
  that catalogs all data and service metadata records for NASA's Earth Observing System Data and Information System (EOSDIS) 
  system and will be the authoritative management system for all EOSDIS metadata. These metadata records are registered, 
  modified, discovered, and accessed through programmatic interfaces leveraging standard protocols and APIs.
  
  CMR is the backend behind Earthdatasearch.  
  [Click here for instructions on deploying EarthdataSearch and CMR](installation/deploy-cmr-earthdata.md)
- DPS: Data Processing service that uses HySDS and AWS.


TODO

- Open Source Community of Practice 
