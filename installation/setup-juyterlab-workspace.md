## EclipseChe ImgSPEC JupyterLab workspace

Eclipse Che workspaces are docker containers that are run on EKS. 
On ImgSPEC we build our custom jupyterlab workspace with extensions that interact with other applications 
to provide all the features of the ADE Platform described earlier in the diagram. 

Here is the link to the repo that contains the custom extensions developed for ImgSPEC project. 
https://gitlab.com/geospec/imgspec-jupyter-ide/-/tree/imgspec-dev

```
NOTE: Soon this will be merged in the open source MAAP repositories. 
```

This repository also contains the dockerfile that is used to build the jupyterlab container.
Using the dockerfile, build the dockerimage and publish it to a docker registry of your choice. 
On ImgSPEC, we use the registry that is in built with our privately hosted gitlab.


### Running a docker image as an EclipseChe workspace

#### Creating a devfile

To run any docker image as an eclipse che workspace, we need a devfile. More information on devfiles can be found [here](https://www.eclipse.org/che/docs/che-7/end-user-guide/authoring-devfiles-version-1/)

A devfile is a YAML file that Che consumes and transforms into a cloud workspace composed of multiple containers. 
Here is the link to the latest devfile used for the ImgSPEC project. 
https://gitlab.com/geospec/che-devfile/-/blob/jupyterlab-v3/miniconda3/devfile.yml


#### Starting up a workspace 

Once we have the devfile and the docker image pushed to a registry, we can create a workspace by going on the EclipseChe
dashboard and selecting custom workspace. Paste the url for the raw devfile and load it. 

![devfile](../images/custom-workspace.png)

After correctly loading the devfile, press Create and Open which will launch the jupyterlab workspace. 


### Next Up

Once you have successfully deployed Eclipse Che and the custom ImgSPEC workspace, we will now deploy the services that 
the imgspec workspace connects with to provide the user with data search, discovery and access.
[Click here for instructions on deploying EarthdataSearch and CMR](deploy-cmr-earthdata.md)
