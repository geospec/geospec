### Earthdata Search and CMR

The ImgSPEC project hosts a smaller custom CMR and earthdatasearch for users of the platform. 

#### Installing ImgSPEC CMR

[Click here](install-cmr.md) for detailed instructions to install CMR

#### Installing OpenSearch Client

To enable serach and discovery of the datasets stored in CMR we need to install the opensearch client. 
The instructions and code to install the same are [here](https://gitlab.com/geospec/nasa-cmr-opensearch)


#### Installing Earthdata Search Client

Please follow the README for [this repository](https://gitlab.com/geospec/earthdata-search) to install EDSC.

This repo contains the code modifications and docker build steps for building the EDSC client. 

Along with EDSC we will also need to install edsc-graphql from [here](https://gitlab.com/geospec/edsc-graphql)


