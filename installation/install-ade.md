# Algorithm Development Environment

### Deploying Eclipse Che

> **_NOTE:_** 
 To be able to deploy this software, it is assumed the reader is familiar with basic deployment techniques on
 Amazon web services, Kubernetes, docker, installing software on Linux operating systems.

Eclipse Che is an open-source, Java-based developer workspace server and Online IDE. It includes a multi-user remote 
development platform. The workspace server comes with a flexible RESTful webservice.

Among the various way eclipse che can be deployed, on ImgSPEC we have deployed EclipseChe 7 using Amazons Elastic
Kubernetes Services (EKS). 
The following documentation on the EclipseChe website is the preferred guide as its most up to date 
with new versions of Che.

https://www.eclipse.org/che/docs/che-7/installation-guide/installing-che-on-aws/#preparing-the-aws-system-for-installing-che_che

Eclipse che comes packaged with keycloak, an open source identity and access management solution. The reader can
choose to user their own preferred method of integrating Single Sign On(SSO) to tie it back to their organizations. 

On the ImgSPEC project, we decided not to pursue integrating third party Single Sign on.

On the NASA Multi-Mission Algorithm and Analysis Platform (MAAP), we decided to integrate SSO using the 
[Apereo CAS](https://www.apereo.org/projects/cas) software and delegating auth to the [Earthdata Login System](https://urs.earthdata.nasa.gov/)     

If your project would like to use a CAS service to integrate different OAuth supported services for sign on, follow
the steps used my the MAAP project as described [here](https://github.com/MAAP-Project/maap-auth-cas/blob/master/README.md)

Once you have successfully installed EclipseChe you should be able to log in and see the dashboard. 

![Dashboard](../images/workspaces.png)


### Eclipse Che Workspaces

Eclipse Che provides developer workspaces with everything you need to code, build, test, run, and debug applications:
- Project source code
- Web-based integrated development environment (IDE)
- Tool dependencies needed by developers to work on a project
- Application runtime: a replica of the environment where the application runs in production

For more info follow - https://www.eclipse.org/che/docs/che-7/end-user-guide/workspaces-overview/

As you can see from the dashboard, Eclipse che offers many built in workspaces. For the ImgSPEC project we created 
custom workspaces based on JupyterLab.

Developer workspaces run within docker containers and to build and deploy these custom docker containers we use the 
gitlab CI/CD. To be able to run custom CI pipelines, we host our own CI runner and connect it to gitlab. 

### Setting up Gitlab with CI/CD

During the initial phases of the project, we hosted our own gitlab service, but due to recent vulnerabilities discovered
 in older version of gitlab, we decided to move to using the public gitlab.com with private repositories. We then 
connected it to our customer gitlab runner hosted on AWS EC2 machines. 


To set up custom gitlab runner:
- Create an EC2 instance in your AWS environment. 
    - On ImgSPEC we have a Centos7 `t3.medium` with `100 GB` EBS attached storage. 
- We then run the gitlab runner as a docker container and register it with the geospec organization on gitlab.com
    - [Click here](https://docs.gitlab.com/runner/install/docker.html) for instructions on setting up your own gitlab runner in docker.
    

### ImgSPEC Deployment Configuration

Eclipse Che - Autoscaling AWS EKS, c5.2xlarge, 50GB EBS per user workspace 
Gitab Runner - AWS EC2 t3.medium, 200GB EBS

    
### Next Up
We will look into creating our base jupyterlab workspace. [Click here](setup-juyterlab-workspace.md)




