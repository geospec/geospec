# ImgSPEC CMR

## Background
This is a modified deployment to run an unofficial CMR instance on EC2 machines. A previous deployment Dockerized the entire CMR system, but because of modifications related to Dockers running within CMR, we deploy CMR to the EC2 machine, effectively running it locally there. An Oracle RDS database is used for data storage. The AWS requirements are as follows:
- c5.xlarge EC2 machine for deployment
- Oracle 12c with 8vCPU, 32GB RAM, 100GB

## Deployment Steps

### Presteps/Checks
- Ensure instruments, locations, platforms, projects, providers, and sciencekeywords lists in `dev-system/resources/kms_examples/` have been updated with full lists. Manually add any unofficial entries such as ImgSPEC to the projects list.
- If using Oracle database, ensure it is up and running with the `guywithnose/sqlplus` Docker image.
  ```
  docker run --rm -it guywithnose/sqlplus:latest /bin/bash
  ```
  ```
  sqlplus '<username>@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=<rds_host>)(PORT=1521))(CONNECT_DATA=(SID=<primary_database>)))'
  ```
- Ensure three Oracle jars (`ojdbc7.jar`, `ons.jar`, `ucp.jar`) are in `oracle-lib/support`. See `/oracle-lib/README.md` for more information about sourcing these jars if needed.
### Update vars
- Create `profiles.clj` in `dev-system`:
  ```
  cp dev-system-profiles.clj.tpl dev-system/profiles.clj
  ```
- Update database vars:
  - `dev-system/profiles.clj`
    - `cmr-db-url`
    - `cmr-sys-dba-username`
    - `cmr-sys-dba-password`
  - `env_vars.sh`
    - `CMR_DB_URL`
    - `CMR_SYS_DBA_USERNAME`
    - `CMR_SYS_DBA_PASSWORD`
- Set ENV variables prior to installing:
  ```
  source ./env_vars.sh
  ```
### Install requirements
- Java 1.8 (openJDK)
- Maven 3.6.3
- [Leiningen](https://leiningen.org)

### Installation
1. Install Oracle jars:
   ```
   ./bin/cmr install oracle-libs
   ```
2. Setup dev (this can be slow)
   ```
   ./bin/cmr setup dev
   ```
3. Build the dev-system uberjar
   ```
   ./bin/cmr build uberjar dev-system
   ```
4. Setup the database
   ```
   ./bin/cmr setup db
   ```
5. Start the dev-system
   ```
   ./bin/cmr start uberjar dev-system
   ```

### Logs
Dev system logs are found at `logs/dev-system.log`.
